﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleClassLibrary
{   //Викладач
    public class Teacher : Person
    {
        //Посада
        private string position;

        public string Position
        {
            get { return position; }
            set { position = value; }
        }

        //Кафедра
        private string chair;

        public string Chair
        {
            get { return chair; }
            set { chair = value; }
        }

        //Вищий навчальний заклад(ВНЗ)
        private string institutionOfHigherEducation;

        public string InstitutionOfHigherEducation
        {
            get { return institutionOfHigherEducation; }
            set { institutionOfHigherEducation = value; }
        }

        //Конструктор за замовчуванням
        public Teacher()
        {

        }

        //Конструктор
        public Teacher(string name, string surname, string dateOfBirth, string position, string chair, string institutionOfHigherEducation) : base(name, surname, dateOfBirth)
        {
            this.position = position;
            this.chair = chair;
            this.institutionOfHigherEducation = institutionOfHigherEducation;
        }

        //Конструктор
        public Teacher(string name, string surname, string position, string chair, string institutionOfHigherEducation) : base(name, surname)
        {
            this.position = position;
            this.chair = chair;
            this.institutionOfHigherEducation = institutionOfHigherEducation;
        }

        //Конструктор копіювання
        public Teacher(Teacher teacher)
        {
            this.Name = teacher.Name;
            this.Surname = teacher.Surname;
            this.DateOfBirth = teacher.DateOfBirth;

            this.Position = teacher.Position;
            this.Chair = teacher.Chair;
            this.InstitutionOfHigherEducation = teacher.InstitutionOfHigherEducation;
        }

        public override void ShowInfo()
        {
            if (DateOfBirth == null)
            {
                Console.WriteLine($"Викладач - {Surname} {Name}");
                Console.WriteLine($"Посада - {Position}");
                Console.WriteLine($"Кафедра - {Chair}");
                Console.WriteLine($"Вищий навчальний заклад - {InstitutionOfHigherEducation}");
            }
            else
            {
                Console.WriteLine($"Викладач - {Surname} {Name}");
                Console.WriteLine($"Дата народження - {DateOfBirth}");
                Console.WriteLine($"Посада - {Position}");
                Console.WriteLine($"Кафедра - {Chair}");
                Console.WriteLine($"Вищий навчальний заклад - {InstitutionOfHigherEducation}");
            }
            
        }
    }
}
