﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleClassLibrary
{   //Людина
    public class Person
    {
        //Ім'я
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        //Прізвище
        private string surname;       

        public string Surname
        {
            get { return surname; }
            set { surname = value; }
        }

        //Дата народження
        private string dateOfBirth;    

        public string DateOfBirth
        {
            get { return dateOfBirth; }
            set { dateOfBirth = value; }
        }

        //Конструктор за замовчуванням
        public Person()
        {

        }

        //Конструктор без дати народження
        public Person(string name, string surname)
        {
            this.name = name;
            this.surname = surname;
        }

        //Конструктор 
        public Person(string name, string surname, string dateOfBirth)
        {
            this.name = name;
            this.surname = surname;
            this.dateOfBirth = dateOfBirth;
        }

        //Конструктор копіювання
        public Person(Person person)
        {
            this.Name = person.Name;
            this.Surname = person.Surname;
            this.DateOfBirth = person.DateOfBirth;
        }

        public virtual void ShowInfo()
        {
            if (DateOfBirth == null)
            {
                Console.WriteLine($"Ім'я - {Name}");
                Console.WriteLine($"Прізвище - {Surname}");
            }
            else
            {
                Console.WriteLine($"Ім'я - {Name}");
                Console.WriteLine($"Прізвище - {Surname}");
                Console.WriteLine($"Дата народження - {DateOfBirth}");
            }
            
        }
    }
}
