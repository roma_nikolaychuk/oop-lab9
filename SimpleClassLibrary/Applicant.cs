﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleClassLibrary
{   //Абітурієнт
    public class Applicant : Person
    {
        //кількість балів сертифікатів ЗНО
        private double amountScoresOfCertificatesOfZNO;

        public double AmountScoresOfCertificatesOfZNO
        {
            get { return amountScoresOfCertificatesOfZNO; }
            set { amountScoresOfCertificatesOfZNO = value; }
        }

        //кількість балів за документ проосвіту
        private double amountScoresOfForTheDocumentOnTheEducation;

        public double AmountScoresOfForTheDocumentOnTheEducation
        {
            get { return amountScoresOfForTheDocumentOnTheEducation; }
            set { amountScoresOfForTheDocumentOnTheEducation = value; }
        }

        //назва загальноосвітнього навчального закладу
        private string nameOfAComprehensiveEducationalInstitution;

        public string NameOfAComprehensiveEducationalInstitution
        {
            get { return nameOfAComprehensiveEducationalInstitution; }
            set { nameOfAComprehensiveEducationalInstitution = value; }
        }

        //Конструктор за замовчуванням
        public Applicant()
        {

        }

        //Конструктор
        public Applicant(string name, string surname, string dateOfBirth, double amountScoresOfCertificatesOfZNO, double amountScoresOfForTheDocumentOnTheEducation, string nameOfAComprehensiveEducationalInstitution) : base(name, surname, dateOfBirth)
        {
            this.amountScoresOfCertificatesOfZNO = amountScoresOfCertificatesOfZNO;
            this.amountScoresOfForTheDocumentOnTheEducation = amountScoresOfForTheDocumentOnTheEducation;
            this.nameOfAComprehensiveEducationalInstitution = nameOfAComprehensiveEducationalInstitution;
        }

        //Конструктор
        public Applicant(string name, string surname, double amountScoresOfCertificatesOfZNO, double amountScoresOfForTheDocumentOnTheEducation, string nameOfAComprehensiveEducationalInstitution) : base(name, surname)
        {
            this.amountScoresOfCertificatesOfZNO = amountScoresOfCertificatesOfZNO;
            this.amountScoresOfForTheDocumentOnTheEducation = amountScoresOfForTheDocumentOnTheEducation;
            this.nameOfAComprehensiveEducationalInstitution = nameOfAComprehensiveEducationalInstitution;
        }

        //Конструктор копіювання
        public Applicant(Applicant applicant)
        {
            this.Name = applicant.Name;
            this.Surname = applicant.Surname;
            this.DateOfBirth = applicant.DateOfBirth;

            this.AmountScoresOfCertificatesOfZNO = applicant.AmountScoresOfCertificatesOfZNO;
            this.AmountScoresOfForTheDocumentOnTheEducation = applicant.AmountScoresOfForTheDocumentOnTheEducation;
            this.NameOfAComprehensiveEducationalInstitution = applicant.NameOfAComprehensiveEducationalInstitution;
        }

        public override void ShowInfo()
        {
            if (DateOfBirth == null)
            {
                Console.WriteLine($"Абітурієнт - {Surname} {Name}");
                Console.WriteLine($"Кількість балів серфітикатів ЗНО - {AmountScoresOfCertificatesOfZNO}");
                Console.WriteLine($"Кількість балів за документ про освіту - {AmountScoresOfForTheDocumentOnTheEducation}");
                Console.WriteLine($"Назва загальноосвітнього навчального закладу - {NameOfAComprehensiveEducationalInstitution}");
            }
            else
            {
                Console.WriteLine($"Абітурієнт - {Name} {Surname}");
                Console.WriteLine($"Дата народження - {DateOfBirth}");
                Console.WriteLine($"Кількість балів серфітикатів ЗНО - {AmountScoresOfCertificatesOfZNO}");
                Console.WriteLine($"Кількість балів за документ про освіту - {AmountScoresOfForTheDocumentOnTheEducation}");
                Console.WriteLine($"Назва загальноосвітнього навчального закладу - {NameOfAComprehensiveEducationalInstitution}");

            }
            
        }
    }
}
