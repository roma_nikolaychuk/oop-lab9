﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleClassLibrary
{
    public class LibraryUser : Student
    {
        //номер читацького квитка
        private int numberOfReadersTicket;

        public int NumberOfReadersTicket
        {
            get { return numberOfReadersTicket; }
            set { numberOfReadersTicket = value; }
        }

        //дата видачі
        private string dateOfIssue;

        public string DateOfIssue
        {
            get { return dateOfIssue; }
            set { dateOfIssue = value; }
        }

        //розмір щомісячного читацького внеску
        private double monthlySizeReaderContribution;

        public double MonthlySizeReaderContribution
        {
            get { return monthlySizeReaderContribution; }
            set { monthlySizeReaderContribution = value; }
        }

        //Конструктор за замовчуванням
        public LibraryUser()
        {

        }

        //Конструктор
        public LibraryUser(string name, string surname, int course, string group, string faculty, string institutionOfHigherEducation, int numberOfReadersTicket, string dateOfIssue, double monthlySizeReaderContribution) : base(name, surname, course, group, faculty, institutionOfHigherEducation)
        {
            this.numberOfReadersTicket = numberOfReadersTicket;
            this.dateOfIssue = dateOfIssue;
            this.monthlySizeReaderContribution = monthlySizeReaderContribution;
        }

        //Конструктор
        public LibraryUser(string name, string surname, string dateOfBirth, int course, string group, string faculty, string institutionOfHigherEducation, int numberOfReadersTicket, string dateOfIssue, double monthlySizeReaderContribution) : base(name, surname, dateOfBirth, course, group, faculty, institutionOfHigherEducation)
        {
            this.numberOfReadersTicket = numberOfReadersTicket;
            this.dateOfIssue = dateOfIssue;
            this.monthlySizeReaderContribution = monthlySizeReaderContribution;
        }

        //Конструктор копіювання
        public LibraryUser(LibraryUser libraryUser)
        {
            this.Name = libraryUser.Name;
            this.Surname = libraryUser.Surname;
            this.DateOfBirth = libraryUser.DateOfBirth;

            this.NumberOfReadersTicket = libraryUser.NumberOfReadersTicket;
            this.DateOfIssue = libraryUser.DateOfIssue;
            this.MonthlySizeReaderContribution = libraryUser.MonthlySizeReaderContribution;
        }

        public override void ShowInfo()
        {
            if (DateOfBirth == null)
            {
                Console.WriteLine($"Користувач бібліотеки - {Surname} {Name}");
                Console.WriteLine($"Курс - {Course}");
                Console.WriteLine($"Група - {Group}");
                Console.WriteLine($"Факультет - {Faculty}");
                Console.WriteLine($"ВНЗ - {InstitutionOfHigherEducation}");
                Console.WriteLine($"Номер читацького квитка - {NumberOfReadersTicket}");
                Console.WriteLine($"Дата видачі - {DateOfIssue}");
                Console.WriteLine($"Розмір щомісячного читацького внеску - {MonthlySizeReaderContribution} грн");
            }
            else
            {
                Console.WriteLine($"Користувач бібліотеки - {Surname} {Name}");
                Console.WriteLine($"Дата народження - {DateOfBirth}");
                Console.WriteLine($"Курс - {Course}");
                Console.WriteLine($"Група - {Group}");
                Console.WriteLine($"Факультет - {Faculty}");
                Console.WriteLine($"ВНЗ - {InstitutionOfHigherEducation}");
                Console.WriteLine($"Номер читацького квитка - {NumberOfReadersTicket}");
                Console.WriteLine($"Дата видачі - {DateOfIssue}");
                Console.WriteLine($"Розмір щомісячного читацького внеску - {MonthlySizeReaderContribution} грн");
            }
            
        }
    }
}
