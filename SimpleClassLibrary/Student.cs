﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleClassLibrary
{   //Студент
    public class Student : Person
    {
        //Курс
        private int course;

        public int Course
        {
            get { return course; }
            set { course = value; }
        }

        //Група
        private string group;

        public string Group
        {
            get { return group; }
            set { group = value; }
        }

        //Факультет
        private string faculty;

        public string Faculty
        {
            get { return faculty; }
            set { faculty = value; }
        }

        //Вищий навчальний заклад(ВНЗ)
        private string institutionOfHigherEducation;

        public string InstitutionOfHigherEducation
        {
            get { return institutionOfHigherEducation; }
            set { institutionOfHigherEducation = value; }
        }

        //Конструктор за замовчуванням
        public Student()
        {

        }

        public Student(int course, string group, string faculty, string institutionOfHigherEducation)
        {
            this.course = course;
            this.group = group;
            this.faculty = faculty;
            this.institutionOfHigherEducation = institutionOfHigherEducation;
        }

        //Конструктор
        public Student(string name, string surname, string dateOfBirth, int course, string group, string faculty, string institutionOfHigherEducation) : base(name, surname, dateOfBirth)
        {
            this.course = course;
            this.group = group;
            this.faculty = faculty;
            this.institutionOfHigherEducation = institutionOfHigherEducation;
        }

        //Конструктор
        public Student(string name, string surname, int course, string group, string faculty, string institutionOfHigherEducation) : base(name, surname)
        {
            this.course = course;
            this.group = group;
            this.faculty = faculty;
            this.institutionOfHigherEducation = institutionOfHigherEducation;
        }

        //Конструктор копіювання
        public Student(Student student)
        {
            this.Name = student.Name;
            this.Surname = student.Surname;
            this.DateOfBirth = student.DateOfBirth;

            this.Course = student.Course;
            this.Group = student.Group;
            this.Faculty = student.Faculty;
            this.InstitutionOfHigherEducation = student.InstitutionOfHigherEducation;
        }

        public override void ShowInfo()
        {
            if(DateOfBirth == null)
            {
                Console.WriteLine($"Студент -  {Surname} {Name}");
                Console.WriteLine($"Курс - {Course}");
                Console.WriteLine($"Група - {Group}");
                Console.WriteLine($"Факультет - {Faculty}");
                Console.WriteLine($"ВНЗ - {InstitutionOfHigherEducation}");
            }
            else
            {
            Console.WriteLine($"Студент - {Surname} {Name}");
            Console.WriteLine($"Дата народження - {DateOfBirth}");
            Console.WriteLine($"Курс - {Course}");
            Console.WriteLine($"Група - {Group}");
            Console.WriteLine($"Факультет - {Faculty}");
            Console.WriteLine($"ВНЗ - {InstitutionOfHigherEducation}");
            }
            
        }
    }
}
