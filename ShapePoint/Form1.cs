﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShapePoint
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonToSpit_Click(object sender, EventArgs e)
        {
            //Прямокутник
            Graphics g = Graphics.FromHwnd(pictureBox.Handle);
            Pen rectangle = new Pen(Color.DarkSalmon, 1);
            Random random = new Random();

            int x = random.Next(0, 300);
            int y = random.Next(0, 300);
          
            int width = random.Next(0, 250);
            int height = random.Next(0, 250);
           
            g.DrawRectangle(rectangle, x, y, width, height);

            //Лінія
            int x1 = random.Next(0, 400);
            int y1 = random.Next(0, 400);
            int x2 = random.Next(0, 400);
            int y2 = random.Next(0, 400);

            Pen pen1 = new Pen(Color.Red);
            g.DrawLine(pen1, x1, y1, x2, y2);

            //Еліпс
            g.DrawEllipse(new Pen(Color.Black), new Rectangle(x1, y1, x2, y2));

            //Коло
            g.DrawEllipse(new Pen(Color.Black), new Rectangle(x1, y1, x, x));

        }

        private void Form1_Load(object sender, EventArgs e)
        {
         
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            pictureBox.Image = null;
        
        }
    }
}
