﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    class Circle : Point
    {
        private double x2;

        public double X2
        {
            get { return x2; }
            set { x2 = value; }
        }

        private double y2;

        public double Y2
        {
            get { return y2; }
            set { y2 = value; }
        }
        public Circle()
        {

        }

        public Circle(double x, double y, double x2, double y2) : base(x, y)
        {
            this.x2 = x2;
            this.y2 = y2;
        }

        public Circle(Circle circle)
        {
            this.X = circle.X;
            this.Y = circle.Y;
            this.X1 = circle.X1;
            this.Y1 = circle.Y1;
            this.X2 = circle.X2;
            this.Y2 = circle.Y2;
        }

    }
}
