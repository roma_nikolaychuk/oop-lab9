﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ClassLibrary
{
    class Rectangle : Line
    {
        private double width;

        public double Width
        {
            get { return width; }
            set { width = value; }
        }

        private double height;

        public double Height
        {
            get { return height; }
            set { height = value; }
        }

        public Rectangle()
        {
            
        }

        public Rectangle(double x1, double y1, double x2, double y2, double width, double height) : base(x1, y1, x2, y2)
        {
            this.width = width;
            this.height = height;
        }

        public Rectangle(Rectangle rectangle)
        {
            this.Width = rectangle.Width;
            this.Height = rectangle.Height;
        }
    }
}
