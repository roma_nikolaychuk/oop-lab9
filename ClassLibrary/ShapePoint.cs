﻿using System;
using System.Drawing;
using System.Security.Cryptography;

namespace ClassLibrary
{
    public class ShapePoint
    {
        private double x1;
        
        public double X1
        {
            get { return x1; }
            set { x1 = value; }
        }
       
        private double y1;

        public double Y1
        {
            get { return y1; }
            set { y1 = value; }
        }


        public ShapePoint()
        {

        }

        public ShapePoint(double x1, double y1)
        {
            this.x1 = x1;
            this.y1 = y1;
        }

        public ShapePoint(ShapePoint shapePoint)
        {
            this.X1 = shapePoint.X1;
            this.Y1 = shapePoint.Y1;
        }
    }


}
