﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;

namespace ClassLibrary
{
    class Point : ShapePoint
    {
        private double x;

        public double X
        {
            get { return x; }
            set { x = value; }
        }

        private double y;

        public double Y
        {
            get { return y; }
            set { y = value; }
        }
        public Point()
        {

        }

        public Point(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        public Point(double x1, double y1, double x, double y) : base(x1,y1)
        {
            this.x = x1;
            this.y = y1;
        }

        public Point(Point point)
        {
            this.X = point.X1;
            this.Y = point.Y1;
            this.X1 = point.X1;
            this.Y1 = point.Y1;
        }
    }
}
