﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    class Line : Point
    {
        private double x2;
        private double y2;

        public double X2 { get { return x2; } set { x2 = value; } }
        public double Y2 { get { return y2; } set { y2 = value; } }

        public Line()
        {

        }

        public Line(double x1, double y1, double x2, double y2) : base(x1,y1)
        {
            this.x2 = x2;
            this.y2 = y2;
        }

        public Line(Line line)
        {
            this.X1 = line.X1;
            this.Y1 = line.Y1;
            this.X2 = line.X2;
            this.Y2 = line.Y2;
        }
    }
}
