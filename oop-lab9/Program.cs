﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleClassLibrary;

namespace oop_lab9
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            
            Console.WriteLine("****************************");

            Person person_nikolaychuk = new Person("Роман", "Ніколайчук", "13.10.2000");
            person_nikolaychuk.ShowInfo();   

            Console.WriteLine("****************************");
            Console.WriteLine();
            Console.WriteLine("****************************");

            Person person_kravchuk = new Person("Олег", "Кравчук");
            person_kravchuk.ShowInfo();    

            Console.WriteLine("****************************");
            Console.WriteLine();
            Console.WriteLine("***************************************************");

            Applicant applicant_nikolaychuk = new Applicant("Роман", "Ніколайчук", "13.10.2000", 170.952, 9.5, "ТНТУ");
            applicant_nikolaychuk.ShowInfo();

            Console.WriteLine("***************************************************");
            Console.WriteLine();
            Console.WriteLine("***************************************************");

            Applicant applicant_kravchuk = new Applicant("Олег", "Кравчук", 174.85, 10.6, "ТНТУ");
            applicant_kravchuk.ShowInfo();
          
            Console.WriteLine("***************************************************");
            Console.WriteLine();
            Console.WriteLine("****************************");

            Student student_nikolaychuk = new Student("Роман","Ніколайчук", 1 , "СН-21", "ФІС", "ТНТУ");
            student_nikolaychuk.ShowInfo();

            Console.WriteLine("****************************");
            Console.WriteLine();
            Console.WriteLine("****************************");

            Student student_kravchuk = new Student("Олег", "Кравчук","16.08.2000", 1, "СН-21", "ФІС", "ТНТУ");
            student_kravchuk.ShowInfo();

            Console.WriteLine("****************************");
            Console.WriteLine();
            Console.WriteLine("***************************************************");

            LibraryUser libraryUser_nikolaychuk = new LibraryUser("Олег", "Кравчук", 665, "СН-21", "ФІС", "ТНТУ", 1, "11.09.2017", 25);
            libraryUser_nikolaychuk.ShowInfo();

            Console.WriteLine("***************************************************");
            Console.WriteLine();
            Console.WriteLine("***************************************************");

            libraryUser_nikolaychuk = new LibraryUser("Роман", "Ніколайчук", "13.10.2000", 666, "СН-21", "ФІС", "ТНТУ", 1, "08.09.2017", 25);
            libraryUser_nikolaychuk.ShowInfo();

            Console.WriteLine("***************************************************");
            Console.WriteLine();
            Console.WriteLine("***************************************************");

            Teacher teacher_german = new Teacher("Олег", "Герман", "Професор", "Українознавства і філософії", "ТНТУ");
            teacher_german.ShowInfo();

            Console.WriteLine("***************************************************");
            Console.WriteLine();
            Console.WriteLine("***************************************************");

            Teacher teacher_kryven = new Teacher("Василь", "Кривень","01.07.1951", "Професор", "Математичних методів в інженерії", "ТНТУ");
            teacher_kryven.ShowInfo();

            Console.WriteLine("***************************************************");

            Console.ReadKey();
        }
    }
}
